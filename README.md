# README #

This package sets up the Dashboard in the Content Section of Umbraco. By default a Welcome page that has a logo & welcome message, and 2 extra tabs for Last Edits & SentToPublish pages.

We recommend to costumize the welcome.html page in (\umbraco\dashboard\NuGet\TamTam.NuGet.Umbraco.DashboardExtension\)

### PACKAGE ###

The package adds the following items to your solution:

* 2 User Controls (ascx, cs & designer.cs) for displaying teh LAst Edited & Send to Publish pages
* 1 Dashboard config file that will overwritte your current Dashboard file

### ISSUES ADDRESSED ###

* Release 1.0.0 no issues (yet) 
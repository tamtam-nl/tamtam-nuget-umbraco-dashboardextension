﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SendToPublish.ascx.cs" Inherits="TamTam.NuGet.Umbraco.DashboardExtension.SendToPublish" %>
<h3>Saved & Send to Publishing</h3>
<ul class="nav nav-stacked">
<asp:Repeater id="Repeater1" runat="server">
	<ItemTemplate>
		<li><%# PrintNodeName(DataBinder.Eval(Container.DataItem, "NodeId"), DataBinder.Eval(Container.DataItem, "datestamp")) %></li>
	</ItemTemplate>
</asp:Repeater>
</ul>

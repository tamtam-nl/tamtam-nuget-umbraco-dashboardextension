﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using umbraco.NodeFactory;
using umbraco.BusinessLogic;
using System;
using Umbraco.Core.IO;
using umbraco.cms.businesslogic.web;
using Umbraco.Core.Services;
using Umbraco.Core.Persistence;

namespace TamTam.NuGet.Umbraco.DashboardExtension
{
    public partial class SendToPublish : System.Web.UI.UserControl
    {
        // Find current user
        private System.Collections.ArrayList printedIds = new System.Collections.ArrayList();
        private int count = 0;
        public int MaxRecords { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (MaxRecords == 0)
                MaxRecords = 30;

            Repeater1.DataSource = Log.GetLogReader(LogTypes.SendToPublish, DateTime.Now.Subtract(new TimeSpan(7, 0, 0, 0, 0)));
            Repeater1.DataBind();
        }

        public string PrintNodeName(object NodeId, object Date)
        {
            var userService = new UserService(new RepositoryFactory());            

            if (!printedIds.Contains(NodeId) && count < MaxRecords)
            {
                printedIds.Add(NodeId);
                try
                {
                    //Umbraco.Web.UmbracoHelper helper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current );
                    //dynamic node = helper.Content(NodeId.ToString());
                    Document node = new Document(int.Parse(NodeId.ToString()));
                    count++;
                    return
                    "<a target=\"blank\" href=\"/umbraco/#/content/content/edit/" + NodeId.ToString() + "\" style=\"text-decoration: none\"><img src=\"" + IOHelper.ResolveUrl(SystemDirectories.Umbraco) + "/images/listItemOrange.gif\" align=\"absmiddle\" border=\"0\"/> " + node.Text + "</a> <p> " + userService.GetById(node.UserId).Name + " - " + DateTime.Parse(Date.ToString().ToString()) + "</p><br/>";
                }
                catch (Exception ex)
                {
                    return "";
                }

            }
            else
                return "";
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
    }
}